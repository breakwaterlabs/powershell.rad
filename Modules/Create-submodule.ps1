function new-RADModule {
    Param(
        [String]$Name,
        [String]$Description,
        [String[]]$Tags = @('Automation', 'ActiveDirectory', 'RBAC')
    )
    Begin {
        $StaticParams = @{
            Author = "John@Breakwaterlabs.net"
            CompanyName = "Breakwaterlabs.net"
            Copyright = '(c) 2023 John@breakwaterlabs.net. All rights reserved.'
            PowerShellVersion = '5.0'
            LicenseUri = 'https://gitlab.com/breakwaterlabs/powershell.rad/-/blob/main/LICENSE'
            ProjectUri = 'https://gitlab.com/breakwaterlabs/powershell.rad/'
            HelpInfoUri = 'https://gitlab.com/breakwaterlabs/powershell.rad/'
            PrivateData = @{
                SchemaVersion = 1
                NamesVersion = 1
                APIVersion = 1
            }
        }
    }
    Process {
        $DynamicParams = @{
            Tags = $Tags
            Description = $Description
            DefaultCommandPrefix = "RAD"
            ReleaseNotes = "First release"
            path = ".\$name\$name.psd1"
        }
        if (-not (test-path .\$name)) {
            new-item -itemType Directory -name $name
        }
        new-moduleManifest @DynamicParams @StaticParams
    }
    End {

    }
}