# RAD PKI Module

## Overview

This module attempts to implement RBAC controls for PKI. It was designed with Active Directory Certificate Services in mind, but the core Roles and Rights that it creates should be relevant even with other PKIs. 



If it is installed with the `-IncludeRootDSEModifications` flag, it will modify objects and DACLs under `CN=Public Key Services,CN=Services,CN=[CONFIGURATION_NAMING_CONTEXT]`. For example,

- Create new certificate templates prefixed as `_RAD.PKI-` with permissions assigned to relevant rights
- Ensure that the OID for `Remote Desktop Authentication` (`1.3.6.1.4.1.311.54.1.2`) has been created


## RBAC theory

The Rights / Roles are designed to make it difficult for one account have excessively dangerous access.  For instance, if the `PKI-Admin` attempts to make life easy by adding also taking  `PKI-Issue` or `PKI-RequestHTTPS` rights, they will find themselves locked out of the CA due to the deny ACLs.

The theory is that sysadmins-- being human-- may be prone to bending the rules slightly to hit a deadline or solve a time-sensitive issue, but they are unlikely to make major changes to circumvent DACLs. These controls provide guardrails and a baseline for audits.

### Rights
The rights (templated security groups) created by this template creates are as follows (✅ = Allow; ❌ = Deny)

| Name | Enroll / Submit CSR | Issue Certs | Write Templates | Manage CA | Description |
| -- | -- | -- | -- | -- | -- |
| PKI-RequestAll | ✅ | ❌ | ❌ | ❌ |Allow submitting arbitrary CSRs | 
| PKI-RequestHTTPS | ✅ | - | ❌ | ❌ |Allow requesting specific templates with Server Auth / RDP EKUs. | 
| PKI-Issue | - | ✅ | ❌ | - | Issue certs from CA (Must be set on per-CA basis) | 
| PKI-ManageTemplates | - | - | ✅ | - | Modify Template objects in AD | 
| PKI-ManageCA | ❌ | - | - | ✅ | Manage CA Policy (Must be set on per-CA basis) | 

__TODO:__ Can CA manage be set in DACLs in AD????

### Roles
The following roles are provided:
| Role | Membership | Request (ServerAuth) | Request (Arbitrary) | Issue | Write Templates | Manage CA | 
| -- | -- | -- | -- | -- | -- | -- | 
| PKI-User | `PKI-RequestAll` | ✅ | ✅ | ❌ | ❌ | ❌ |
| PKI-Operator | `PKI-RequestHTTPS`; `PKI-Issue` | ✅ | ❌ | ✅ | ❌ | ❌ |
| PKI-AdminOperators | `PKI-Issue`; `PKI-ManageCA` | ❌ | ❌ | ✅ | ❌ | ✅ |
| PKI-SuperAdmin | `PKI-ManageCA`; `PKI-ManageTemplates` | ❌ | ❌ | - | ✅ | ✅ |

### Role Risk Analysis

The analysis below assumes an attacker has gained control over the listed role, and has no additional access (unless otherwise noted).

__PKI-User__: Can flood the CA request interface. Escalation requires tricking an issuing role into approving a request.

__PKI-Operator__: Can issue arbitrary server auth certificates. Escalation requires gaining access to perform Man-in-the-middle attacks at which point any domain TLS comms could be compromised until the role is removed / locked down and revocation is performed. The impact of this depends on what kinds of authentication are used on the network (Basic, NTLM, Kerberos) but could escalate into credential theft if weak forms of authentication are used.

__PKI-AdminOperator__: Can publish any available template on the domain and remove issuance requirements on the CA. It is likely that this role allows issuing arbitrary certificates and is likely equivalent to domain admin in a default ADCS Enterprise CA deployment; while it does not grant direct access to AD objects, it can likely create and use PKInit certificates to authenticate as a domain admin.
It is possible to set up roadblocks like locking down certificate templates (which this role cannot modify). However, because this role owns the CA it can likely bypass those administrative barriers locally. 
The distinction between this role and PKI-Admin should be seen as a guardrail to prevent mistakes like updating templates to meet vendor whim without going through the proper channels.

__PKI-Admin__: Can create and publish arbitrary templates. It is likely that this is intrinsically and inseperably equivalent to domain admin for the same reasons as `PKI-AdminOperator`, and because it also gains direct access to parts of AD.
This role has value in making it difficult to make bad decisions, and making it painful to over-accumulate permissions.


## Certificate Templates

Out of the box, the following templates are provided.

| Name | Expire (days)| EKU | Subject | Issuance | Algorithms | Enroll | AutoEnroll | 
| -- |  -- | -- | -- | -- | -- | -- | -- | 
| _RAD-AutoRSA | 30 | ServerAuth, RDP | From AD | Automatic | RSA 3072 / SHA384 | Domain Computers | Domain Computers | 
| _RAD-AutoECC | 30 | ServerAuth, RDP | From AD | Automatic | ECDSA 256 / SHA256 | Domain Computers | Domain Computers | 
| _RAD-RSAWebServer | 365 | ServerAuth | Supply in request | Manual | RSA 2048 / SHA256 | PKI-RequestHTTPS, PKI-RequestAll | - |
| _RAD-ECCWebServer | 365 | ServerAuth | Supply in request | Manual | ECDSA 256 / SHA256 | PKI-RequestHTTPS, PKI-RequestAll | - |

The intent is that most domain computers should automatically gain a certificate with a lifetime that matches the default computer password lifetime. Compromising the certificate should thus have no greater impact than compromising the computer's SYSTEM account.

This makes it easy to secure services without requiring manual intervention in the majority of cases. Arbitrary SAN usages are supported with a little more effort and would work very well with load balancers.