$GoodCerts = get-currentValidCertificates
$CertBindings = @(
    @{
        Name = "WinRM HTTPS (Remote Powershell)"
        CertPreferenceOrder = @(
            $GoodCerts.Newest.RDP
            $GoodCerts.Newest.ECC
            $GoodCerts.Newest.RSA
        )
        Current = {
            (get-item -path WSMan:\Localhost\Service\CertificateThumbprint).value
        }
        UpdateFunction = {
            Param(
                [Parameter(Mandatory, ValueFromPipeline)]
                [System.Security.Cryptography.X509Certificates.X509Certificate2]$NewCert
            )
            set-wsmaninstance -ResourceURI winrm/config/service -valueSet @{
                CertificateThumbprint = $NewCert.Thumbprint
            }
            winrm delete winrm/config/listener?Address=*+Transport=HTTPS
            winrm quickconfig -transport:HTTPS -quiet
            restart-service WinRM -force
        }
    }
    @{
        Name = "WMSvc - IIS Management Certs"
        CertPreferenceOrder = @(
            $GoodCerts.Newest.RDP
            $GoodCerts.Newest.ECC
            $GoodCerts.Newest.RSA
        )[0]
        Current = {((netsh http show sslcert 0.0.0.0:8172 | select-string "Certificate Hash") -split ":")[1].trim()}
        UpdateFunction = {
            Param(
                [Parameter(Mandatory, ValueFromPipeline)]
                [System.Security.Cryptography.X509Certificates.X509Certificate2]$NewCert
            )
            Stop-Service wmsvc
            $strGuid = New-Guid
            netsh http delete sslcert ipport=0.0.0.0:8172
            netsh http add sslcert ipport=0.0.0.0:8172 certhash=$($NewCert.Thumbprint) appid=`{$strGuid`} certstorename="MY"
        
            #convert thumbprint to bytes and update registry
            $bytes = for($i = 0; $i -lt $NewCert.Thumbprint.Length; $i += 2) { [convert]::ToByte($NewCert.Thumbprint.SubString($i, 2), 16) }
            Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WebManagement\Server' -Name IPAddress -Value "*";
            Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WebManagement\Server' -Name SslCertificateHash -Value $bytes
            Start-Service wmsvc
        }
        
    }
)

Function get-currentValidCertificates {
    $EKU = @{
        "RDP" = "1.3.6.1.4.1.311.54.1.2"
        "ServerAuth" = "1.3.6.1.5.5.7.3.1"
    }
    $ComputerFQDN = [system.net.dns]::GetHostByName($env:computerName).hostname
    $ValidCerts = (get-childItem -path Cert:\LocalMachine\My).Where{
        (
            $_.EnhancedKeyUsageList.ObjectID.Contains($EKU.ServerAuth) -or
            $_.EnhancedKeyUsageList.ObjectID.Contains($EKU.RDP)
        ) -and 
        $_.subject -match "CN=$ComputerFQDN" -and
        $_.DNSNameList -contains $ComputerFQDN -and
        $_.notAfter -gt (get-date) -and
        $_.notBefore -lt (get-Date)
    } | sort-object notAfter -Descending
    @{
        Longest = @{
            RDP = $validCerts.where{$_.EnhancedKeyUsageList.ObjectID.contains($EKU.ServerAuth) -and $_.EnhancedKeyUsageList.ObjectID.contains($EKU.RDP)} | sort-object notAfter -descending -top 1
            ECC = $validCerts.where{$_.EnhancedKeyUsageList.ObjectID.contains($EKU.ServerAuth) -and $_.PublicKey.EncodedKeyValue.Oid.FriendlyName -eq "ECC"} | sort-object notAfter -descending -top 1
            RSA = $validCerts.where{$_.EnhancedKeyUsageList.ObjectID.contains($EKU.ServerAuth) -and $_.PublicKey.EncodedKeyValue.Oid.FriendlyName -eq "RSA"} | sort-object notAfter -descending -top 1
        }
        # Certs most recently issued
        Newest = @{
            RDP = $validCerts.where{$_.EnhancedKeyUsageList.ObjectID.contains($EKU.ServerAuth) -and $_.EnhancedKeyUsageList.ObjectID.contains($EKU.RDP)} | sort-object notBefore -descending -top 1
            ECC = $validCerts.where{$_.EnhancedKeyUsageList.ObjectID.contains($EKU.ServerAuth) -and $_.PublicKey.EncodedKeyValue.Oid.FriendlyName -eq "ECC"} | sort-object notBefore -descending -top 1
            RSA = $validCerts.where{$_.EnhancedKeyUsageList.ObjectID.contains($EKU.ServerAuth) -and $_.PublicKey.EncodedKeyValue.Oid.FriendlyName -eq "RSA"} | sort-object notBefore -descending -top 1
        }
    }
}


Function Rebind-OldCerts {
    Param(
        # Display name of the binding (for logging purposes)
        [String]$BindingName,

        # The certificate that should be used for this binding.
        [System.Security.Cryptography.X509Certificates.X509Certificate2[]]$CertPreferenceOrder,

        # Scriptblock to get Current Certificate Thumbprint
        [scriptblock]$Current,

        # Scriptblock to update thumbprint
        [Scriptblock]$UpdateFunction,

        [Switch]$Force
    )
    Begin {
    }
    Process {
        $DesiredCert = $CertPreferenceOrder[0]
        $ChangeRequired = [bool]$force
        write-host "Force change: $([bool]$force)"
        foreach ($binding in $CertBindings) {
            write-warning "binding: $($Binding.name)"
            $CurrentThumbprint = invoke-command -ScriptBlock $Binding.current
            if ($CurrentThumbprint -ne $binding.Desired.Thumbprint -or $Force) {
                if ($force) {
                    write-warning " - 'Force' switch used."
                } else {
                    write-warning " - Better cert is available."
                }
                write-warning (" - Prior cert thumbprint: {0}" -f $CurrentThumbprint)
                $binding.desired | invoke-command -scriptBlock $Binding.updateFunction
            } else {
                write-warning " - No change needed. "
            }
            write-warning (" - Current Certificate: {0}`r`n Subject: {1}`r`n Validity: {2} - {3}" -f $binding.Desired.Thumbprint, $binding.Desired.subject, $binding.Desired.notbefore, $binding.Desired.notafter)
        }
    }

    End {

    }
}