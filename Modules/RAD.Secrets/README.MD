# (Ab)Using Windows LAPS as a Password Vault

In April 2023, Microsoft updated their legacy LAPS via a set of cumulative updates, schema extensions, and ADMX changes. The new feature, confusingly called Windows LAPS, uses DPAPI-NG to store encrypted Windows administrator passwords under the MS-DS- attribute under AD computer objects. Only users who are a member of the correct security group are able to decrypt the password, regardless of whether they are able to read the LDAP attributes.

Of course, the most significant limitation of this feature is that it only *officially* supports Windows clients via GPO.

## Reverse Engineering

First things first: much credit goes to [schorschii](https://github.com/schorschii) and their excellent [LAPS4LINUX](https://github.com/schorschii/LAPS4LINUX) project which demonstrated a python implementation of Windows LAPS.

While sparse, Microsoft's documentation[^1] provides crucial details. The important attributes are `msLAPS-Password`, which contains unencrypted passwords, and `msLAPS-EncryptedPassword` which is just the `[DpapiNgUtil]::Protect()`'ed version of that attribute. The password is formatted as a compressed JSON as follows:

```JSON
{
  "n":"Administrator",     // Username
  "t":"1d8161b41c41cde",   // UTC update time as a 64-bit hexadecimal number (hex-formatted filetime)
  "p":"A6a3#7%eb!57be4a4B" // Password
}
```
Once the string is formatted, CNG DPAPI is used via the `ncrypt.dll` method `ProtectSecret()`[^2].


[^1]: https://learn.microsoft.com/en-us/windows-server/identity/laps/laps-technical-reference "LAPS Technical Reference"
[^2]: https://learn.microsoft.com/en-us/windows/win32/api/ncryptprotect/nf-ncryptprotect-ncryptprotectsecret 