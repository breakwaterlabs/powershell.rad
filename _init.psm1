$ADDomain = get-ADDomain
$domainBase = $ADDomain.DistinguishedName
$RightsName = "Right"
$RolesName = "Role"
$UsersOU = "Users"
$SensitiveUsersOU = "AdminAccounts"
$ComputersOU = "NewComputers"
$SleepTimeout = 20
$sleepLength = 2

# Get public and private function definition files
    $public = @( Get-ChildItem -path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue | sort fullName)
    $private = @( Get-ChildItem -path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue | sort fullName)
    $variables = @( Get-ChildItem -path $PSScriptRoot\vars\*.ps1 -ErrorAction SilentlyContinue | sort fullName)
	Import-Module "$PSScriptRoot\modules\GPRegistryPolicy\GPRegistryPolicy.psm1" -DisableNameChecking
	Import-Module "$PSScriptRoot\modules\GPRegistryPolicy\GPRegistryPolicyParser.psm1" -DisableNameChecking


#Dot Source the files
    foreach ($import in @($Private + $Public)) 
    {
        Try
        {
            . $import.fullname
        }
        Catch 
        {
            write-error -Message "Failed to import function $($import.fullname): $_"
        }
    }
    $ObjectGUIDs = get-ADObjectGUIDs
    foreach ($import in @($variables)) 
    {
        Try
        {
            . $import.fullname
        }
        Catch 
        {
            write-error -Message "Failed to import function $($import.fullname): $_"
        }
    }

    

# Stuff to do
    # Read in or create an initial config file and variable
    # Export functions that are WIP
    # set variables visible to module
#$ConfirmPreference = "High"

Export-moduleMember -function $public.basename
